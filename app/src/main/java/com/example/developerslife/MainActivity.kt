package com.example.developerslife

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.developerslife.databinding.ActivityMainBinding
import com.example.developerslife.ui.main.PlaceholderFragment
import com.example.developerslife.ui.main.ProfilePagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val pages = listOf(
            Pair(PlaceholderFragment(), getString(R.string.last_title)),
            Pair(PlaceholderFragment(), getString(R.string.best_title)),
            Pair(PlaceholderFragment(), getString(R.string.hot_title)),
        )

        binding.viewPager.adapter = ProfilePagerAdapter(this,  pages.map { it.first })
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = pages[position].second
        }.attach()

    }
}
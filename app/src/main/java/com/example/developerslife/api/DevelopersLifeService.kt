package com.example.developerslife.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DevelopersLifeService {

    @GET("/{type}/{page}")
    suspend fun getContent(
        @Path("type") type: String,
        @Path("page") page: Int,
        @Query("json") json: Boolean = true
    ): NetworkContent

    companion object {
        private const val BASE_URL = "https://developerslife.ru"

        fun create(): DevelopersLifeService {
            val logger =
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(DevelopersLifeService::class.java)
        }
    }
}
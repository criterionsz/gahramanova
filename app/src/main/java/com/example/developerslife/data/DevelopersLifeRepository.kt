package com.example.developerslife.data

import com.example.developerslife.api.Content
import com.example.developerslife.api.DevelopersLifeService
import com.example.developerslife.api.NetworkContent

class DevelopersLifeRepository(private val service: DevelopersLifeService) {
    var latest = mutableListOf<Content>()
    var best = mutableListOf<Content>()
    var hot = mutableListOf<Content>()
    var posLates = 0
    var posBest = 0
    var posHot = 0
    suspend fun getNextLatestContent(): Content {
        return if (posLates < latest.size) {
            latest[posLates++]
        } else {
            var response: NetworkContent
            var temp: List<Content>
            var countResponses = 0
            do {
                response = service.getContent(
                    "latest",
                    posLates
                )
                temp = response.result.map {
                    Content(
                        it.description,
                        it.gifURL?.replace("http://", "https://")
                    )
                }.filter { !it.gifURL.isNullOrEmpty() }
                countResponses += 1
                if (countResponses > 10) break
            } while (temp.isNullOrEmpty())
            latest.addAll(temp)
            posLates++
            latest[posLates]
        }
    }

    fun getPrevLatestContent(): Content? {
        if (posLates == 0)
            return null
        return latest[--posLates - 1]
    }

    suspend fun getNextBestContent(): Content {
        return if (posBest < best.size) {
            best[posBest++]
        } else {
            var response: NetworkContent
            var temp: List<Content>
            var countResponses = 0
            do {
                response = service.getContent(
                    "best",
                    posBest
                )
                temp = response.result.map {
                    Content(
                        it.description,
                        it.gifURL?.replace("http://", "https://")
                    )
                }.filter { !it.gifURL.isNullOrEmpty() }
                countResponses += 1
                if (countResponses > 10) break
            } while (temp.isNullOrEmpty())
            best.addAll(temp)
            posBest++
            best[posBest]
        }
    }

    fun getPrevBestContent(): Content? {
        if (posBest == 0)
            return null
        return best[--posBest - 1]
    }

    suspend fun getNextHotContent(): Content {
        return if (posHot < hot.size) {
            hot[posHot++]
        } else {
            var response: NetworkContent
            var temp: List<Content>
            var countResponses = 0
            do {
                response = service.getContent(
                    "hot",
                    posHot
                )
                temp = response.result.map {
                    Content(
                        it.description,
                        it.gifURL?.replace("http://", "https://")
                    )
                }.filter { !it.gifURL.isNullOrEmpty() }
                countResponses += 1
                if (countResponses > 10) break

            } while (temp.isNullOrEmpty())
            hot.addAll(temp)
            posHot++
            hot[posHot]
        }
    }

    fun getPrevHotContent(): Content? {
        if (posHot == 0)
            return null
        return hot[--posHot - 1]
    }
}
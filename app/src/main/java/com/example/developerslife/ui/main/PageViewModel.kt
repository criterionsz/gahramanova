package com.example.developerslife.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.developerslife.data.DevelopersLifeRepository
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.Exception

class PageViewModel constructor(
    private val repository: DevelopersLifeRepository
) : ViewModel() {
    val detectedContent: LiveData<Pair<String?, String?>>
        get() = _detectedContent
    private val _detectedContent = MutableLiveData<Pair<String?, String?>>()

    val isPrevButtonEnabled: LiveData<Boolean>
        get() = _isPrevButtonEnabled
    private val _isPrevButtonEnabled = MutableLiveData<Boolean>()

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    private var _eventNetworkError = MutableLiveData(false)


    fun getContent(type: Type) = viewModelScope.launch {
        try {
            val response = when (type) {
                Type.LATEST -> {
                    if (repository.posLates != 0) {
                        _isPrevButtonEnabled.value = true
                    }
                    repository.getNextLatestContent()

                }
                Type.BEST -> {
                    repository.getNextBestContent()
                }
                Type.HOT -> {
                    repository.getNextHotContent()
                }
            }
            _detectedContent.value = Pair(response.gifURL, response.description)
            if (_eventNetworkError.value!!) {
                _eventNetworkError.value = false
            }
        } catch (networkError: IOException) {
            _eventNetworkError.value = true
        } catch (e: Exception) {
           //Exception
        }
    }

    fun getPrevContent() = viewModelScope.launch {
        val response = repository.getPrevLatestContent()
        if (repository.posLates - 1 == 0) {
            _isPrevButtonEnabled.value = false
        }
        _detectedContent.value = Pair(response?.gifURL, response?.description)
    }

}

enum class Type {
    LATEST,
    BEST,
    HOT
}
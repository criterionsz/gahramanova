package com.example.developerslife.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.developerslife.R

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class ProfilePagerAdapter(fragment: FragmentActivity, private val pages: List<Fragment>) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return pages.size
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                pages[position].arguments = Bundle().apply {
                    putSerializable(ARG_TABS, Type.LATEST)
                }
            }

            1 -> {
                pages[position].arguments = Bundle().apply {
                    putSerializable(ARG_TABS, Type.BEST)
                }
            }

            2 -> {
                pages[position].arguments = Bundle().apply {
                    putSerializable(ARG_TABS, Type.HOT)
                }
            }

        }
        return pages[position]
    }
}

const val ARG_TABS = "tabs"
package com.example.developerslife.ui.main

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.example.developerslife.databinding.FragmentMainBinding
import com.example.developerslife.di.Injection

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel
    private var _binding: FragmentMainBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var type: Type

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProvider(
            this,
            Injection.provideViewModelFactory()
        ).get(PageViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val root = binding.root

        arguments?.takeIf { it.containsKey(ARG_TABS) }?.apply {
            type = getSerializable(ARG_TABS) as Type
        }
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //init
        pageViewModel.getContent(type)

        binding.next.setOnClickListener {
            pageViewModel.getContent(type)

        }

        binding.back.setOnClickListener {
            binding.progress.visibility = View.VISIBLE
            pageViewModel.getPrevContent()
        }

        pageViewModel.isPrevButtonEnabled.observe(viewLifecycleOwner) {
            binding.back.isEnabled = it
        }


        pageViewModel.detectedContent.observe(viewLifecycleOwner) {
            binding.title.text = it.second
            Glide.with(view)
                .load(it?.first)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean,
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean,
                    ): Boolean {
                        binding.progress.visibility = View.GONE
                        return false
                    }

                })
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.gif)
        }


        binding.back.setOnClickListener {
            pageViewModel.getPrevContent()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}